var number = 1;
var run = true;

function getCost(n,amount) {
  return 2**n;
}

class Generator {
  constructor(n, div) {
    this.id = n;
    this.n = n;
    this.amount = 0;
    this.div = div;
    this.multiplier = 2**n;
    this.updateInner();
    this.div.classList.add("generator");
  }
  updateInner(){
    this.div.innerHTML = `
      <div class="rate">Per minute: ${readable(this.generates,true)}</div>
      <div class="amount">Amount: ${readable(this.amount,false)}</div>
      <div class="cost">Cost: ${readable(this.cost,false)}</div>
      <button class="buy" onclick="buyGenerator(${this.id})">Buy 1</button>
    `;
    this.updateButton();
  }
  get cost(){
    return getCost(this.n, this.amount);
  }
  get generates(){
    return (1-(1/(this.amount+1)))*this.multiplier;
  }
  updateButton(){
    var want = number >= this.cost ? "" : "#444444";
    if (this.div.children[3].style.backgroundColor != want){
      this.div.children[3].style.backgroundColor = want;
    }
  }
}

function readable(num, dec) {
  var s = ["", "K", "M", "B", "T", "Q"];
  if (num===0){return 0;}
  var e = Math.floor(Math.log(num) / Math.log(1000));
  if (e < 0) {return num.toFixed(3)}
  if (e < s.length) {
    if (e == 0 & !dec) {return num;}
    return ((num / 1000**e).toFixed(0) + s[e]);
  } else {
    return ((num / 1000**e).toFixed(0) + "e" + ((e)*3).toString());
  }
}

function buyGenerator(id){
  if (number >= generators[id].cost){
    number -= generators[id].cost;
    generators[id].amount += 1;
    generators[id].updateInner();
  }
}

var tick = 0;

function main(tick){
  var delta = 0;
  for (var i=0; i<generators.length; i++) {
    delta += generators[i].generates;
    if (tick%5 == 0) {generators[i].updateButton();}
  }

  number += delta/1200; // minute/tick = 1200 (20*60)

  if (number > getCost(generators.length-2,0)){
    a();
  }

  document.getElementById("number").innerText = readable(number,true);
  document.getElementById("delta").innerText = readable(delta,true);

  if (run) {
    setTimeout(main, 1000/20, tick+1); // tps: 20
  } else {
    return;
  }
}

function createGenerator(n){
  var div = document.createElement("div");
  var x = new Generator(n, div);
  document.getElementById("generators").appendChild(div);
  return x;
}

var generators = Array();

function a(){
  generators.push(createGenerator(generators.length));
  if (number > getCost(generators.length-2,0)) {a()};
}

a();
main(tick);

function save(){
  var s = number.toString()+",";
  for (var i=0; i<generators.length; i++) {
    s += generators[i].amount.toString()+",";
  }
  document.getElementById("save").value = s;
  document.getElementById("save").style.display = "inline";
}
function showLoad(){
  document.getElementById("load").style.display = "inline";
}
function load(){
  var s = document.getElementById("load-input").value;
  s = s.split(",");
  number = Number(s[0]);
  s.splice(0,1);
  run = false;
  while (generators.length > 0) {
    generators[0].div.remove();
    generators.splice(0,1)
  }
  a();
  for (var i=0; i<(s.length); i++) {
    if (i>=generators.length){a();}
    generators[i].amount = Number(s[i]);
    generators[i].updateInner();
  }
  run = true;
  main();
}